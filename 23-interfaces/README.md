Interfaces in layman terms - "Hey baby if you've got this method then you're my type"

Using interfaces, a value can be represented in multiple types, AKA, polymorphism.

For example, a Dog can walk and bark. If an interface defines method signatures for walk and bark while Dog implements walk and bark methods, then Dog is said to implement that interface.

The primary job of an interface is to provide only method signatures consisting of the method name, input arguments and return types. It is up to a Type (e.g. struct type) to declare methods and implement them.

https://medium.com/rungo/interfaces-in-go-ab1601159b3a

https://www.ardanlabs.com/blog/2015/09/composition-with-go.html