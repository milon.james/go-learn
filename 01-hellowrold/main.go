package main

import "fmt"

func main() {
	fmt.Println("Hello world!")
	foo()
	fmt.Println("I am going to enter an iteration, booyaaahhh!!")
	for i := 0; i < 100; i++ {
		if i%2 == 0 {
			fmt.Println("Hello I am", i, "and I am even")
		} else {
			fmt.Println("Hello I am", i, "and I am odd")
		}
	}
}

func foo() {
	fmt.Println("Hello, I am in foo!")
}
