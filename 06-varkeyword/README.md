If you ever need to declare a variable outside of a function you use 'var' keyword to define variable
Advice - To limit the scope of your variables to as much as possible, try to use short declaration operator.

https://golang.org/ref/spec#The_zero_value