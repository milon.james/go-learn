package main

import (
	"fmt"
)

func main() {

	switch { // An expressionless SWITCH statement is equivalent to BOOLEAN TRUE condition.
	case false:
		fmt.Println("This will not print")
	case true:
		fmt.Println("This will print")
	case !true:
		fmt.Println("This will not print")
	case !false:
		fmt.Println("This will print")
	}

	example1()
	example2()
}

//fallthrough - A "fallthrough" statement transfers control to the first statement of the next case clause in an expression "switch" statement.
// It may be used only as the final non-empty statement in such a clause. 
func example1() {
	switch {
	case false:
		fmt.Println("This will not print")
	case true:
		fmt.Println("This will print")
		fallthrough
	case !true:
		fmt.Println("This will not usually print - but will print as fallthrough used in previous case statement")
	case !false:
		fmt.Println("This will print")
	}
}
func example2() {
	switch "Bond" {
	case "B":
		fmt.Println("B")
	case "Bond":
		fmt.Println("James Bond")
	case "Peter", "Scott": // Multiple conditional in case statement
		fmt.Println("Peter Scott")
	default:
		fmt.Println("Nothing Matched")
	}
}
