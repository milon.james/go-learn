package main

import (
	"fmt"
)

func main() {
	a := 42
	fmt.Println(&a)
	fmt.Printf("%T\n", a)
	fmt.Printf("%T\n", &a) //&a gives you the address
	b := &a
	fmt.Println(b)
	fmt.Printf("%T\n", b)
	fmt.Println(*b) // * gives you the value stored at an address when you have the address, also called de-referencing an address
	fmt.Println(*&a)

	*b = 43 // *b points to the values stored at address &a, which is the value of a. Hence the value of a changes to 43 after this operation
	fmt.Println(a)
}
