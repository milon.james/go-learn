package main

import (
	"fmt"
)

func main() {
	x := 42
	fmt.Println(x)
	x = 45
	fmt.Println(x)
	y := 45 + 34
	fmt.Println(y)
	z := "James, Milon"
	fmt.Println(z)

}
