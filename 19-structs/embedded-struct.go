package main

import (
	"fmt"
)

type person struct {
	first string
	last  string
	age   int
}

type role struct {
	person
	admin bool
}

func main() {

	r1 := role{
		person: person{
			first: "Milon",
			last:  "James",
			age:   32,
		},
		admin: true,
	}

	p2 := person{
		first: "Chris",
		last:  "Raich",
		age:   28,
	}

	fmt.Println(r1)
	fmt.Println(p2)

	fmt.Println(r1.first, r1.last, r1.age, r1.admin)
	fmt.Println(p2.first, p2.last, p2.age)

}
