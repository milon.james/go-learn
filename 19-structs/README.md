https://golang.org/ref/spec#Struct_types

-> Anonymous fields declared in a STRUCT

Anonymous STRUCTS - can be used to avoid code pollution. If we want our STRUCT to use in a small little area in our code
                     we can use Anonymous STRUCTs