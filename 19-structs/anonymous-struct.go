package main

import (
	"fmt"
)

func main() {

	p1 := struct {
		first, last string
		age         int
	}{
		first: "Milon",
		last:  "James",
		age:   32,
	}

	fmt.Println(p1)
}
