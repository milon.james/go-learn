package main

import (
	"fmt"
)

func main() {

	ii := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	sum_total := sum(ii...)
	fmt.Println(sum_total)
	sum_even_total := sum_even(sum, ii...) //callback
	fmt.Println(sum_even_total)

}

func sum(ii ...int) int {
	total := 0
	for _, v := range ii {
		total = total + v
	}

	return total
}

func sum_even(f func(xi ...int) int, vi ...int) int {

	var yi []int
	for _, v := range vi {
		if v%2 == 0 {
			yi = append(yi, v)
		}
	}

	return f(yi...) //returning a function
}
