package main

import (
	"fmt"
)

func main() {
	foo()
	bar("Mico")
	s := woo("Nolim")
	fmt.Println(s)
	x, y := okie("Nolim", "Chris")
	fmt.Println(x)
	fmt.Println(y)
	z := total(1, 2, 3, 4, 5, 6, 7)
	fmt.Println("The sum is", z)
}

func foo() {
	fmt.Println("Welcome to function foo")
}

func bar(s string) {
	fmt.Println("Hello,", s)
}

func woo(s string) string {
	return fmt.Sprint("Hello from woo, ", s)
}

func okie(s string, t string) (string, bool) {
	a := fmt.Sprint(s, t, `says, "Hello"`)
	return a, true
}

func total(n ...int) int { //variadic parameter
	s := 0
	for i, v := range n {
		fmt.Println("For item in index position", i, "we are adding", v, "to the total, which is", s)
		s += v
	}

	return s
}
