package main

import (
	"fmt"
)

func main() {

	n := 6
	fact1 := factorial(n)
	fact2 := loopFactorial(n)
	fmt.Println(fact1)
	fmt.Println(fact2)

}

func factorial(n int) int {
	if n == 0 {
		return 1
	}

	return n * factorial(n-1)
}

func loopFactorial(n int) int {
	total := 1
	for ; n > 0; n-- {
		total *= n
	}
	return total
}
