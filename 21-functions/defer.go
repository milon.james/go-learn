package main

import (
	"fmt"
)

func main() {

	defer foo() //defer execution of function foo() until the surrounding function( which in this case is main()) returns.
	bar()

}

func foo() {
	fmt.Println("FOO")
}

func bar() {
	fmt.Println("BAR")
}
