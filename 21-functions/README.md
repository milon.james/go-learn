Parameters - We declare when defining functions
Arguments - We pass when calling functions

Everything is PASS BY VALUE in functions in Golang

Variadic parameters - zero or more(unlimited) number of parameters - https://golang.org/ref/spec#Passing_arguments_to_..._parameters

Defer statements
----------------
Each time a "defer" statement executes, the function value and parameters to the call are evaluated as usual and saved anew but the actual function is not invoked. Instead, deferred functions are invoked immediately before the surrounding function returns, in the reverse order they were deferred. That is, if the surrounding function returns through an explicit return statement, deferred functions are executed after any result parameters are set by that return statement but before the function returns to its caller. If a deferred function value evaluates to nil, execution panics when the function is invoked, not when the "defer" statement is executed. 

https://golang.org/ref/spec#Defer_statements

Passing a function as an argument is called callback in golang. https://medium.com/@oshankkumar/callback-function-in-golang-df32fa81a4e2

https://gobyexample.com/closures
https://www.calhoun.io/5-useful-ways-to-use-closures-in-go/