package main

import (
	"fmt"
)

func main() {

	defer foo()
	bar()
}

func foo() {
	fmt.Println("Mico")
}

func bar() {
	fmt.Println("Nolim")
}
