package main

import (
	"fmt"
)

func main() {
	result := foo(619)
	fmt.Printf("The variable result is of the type %T\n", result)
	fmt.Println(result())
}

func foo(x int) func() int {
	var y int = x + 5
	z := func() int {
		return y * 100
	}
	fmt.Printf("The function foo() is returning a value of type %T\n", z)
	return z
}
