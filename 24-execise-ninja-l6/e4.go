package main

import (
	"fmt"
)

type person struct {
	first, last string
	age         int
}

func (p person) speak() {
	fmt.Println("Hi, I am", p.first, "and I am", p.age, "years old")
}

func main() {

	p1 := person{
		first: "Mico",
		last:  "Nolim",
		age:   33,
	}

	p1.speak()
}
