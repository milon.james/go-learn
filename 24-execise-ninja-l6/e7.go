package main

import (
	"fmt"
)

func main() {
	result := foo(619)
	fmt.Println(result)
}

func foo(x int) int {
	return (x + 5)
}
