package main

import (
	"fmt"
)

func main() {

	//ananonymous func
	func(n int) {
		fmt.Println("Start anonymous func")
		for i := 1; i <= n; i++ {
			fmt.Println(i)
		}
		fmt.Println("Done")
	}(100)
}
