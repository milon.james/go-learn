package main

import (
	"fmt"
	"math"
)

type circle struct {
	radius float64
}

type square struct {
	length float64
}

func (s circle) area() float64 {

	return (math.Pi * (s.radius * s.radius))
}

func (s square) area() float64 {
	return (s.length * s.length)
}

type shape interface {
	area() float64
}

func info(sh shape) {
	fmt.Println(sh.area())
}

func main() {

	c1 := circle{
		radius: 10,
	}

	s1 := square{
		length: 20,
	}

	info(c1)
	info(s1)

}

// func info() {

// }
