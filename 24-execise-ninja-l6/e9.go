package main

import (
	"fmt"
)

func main() {
	x := 9
	check_result := oddOrEven(factorial, x)
	fmt.Println(check_result)
}

func factorial(n int) int {
	if n == 1 {
		return 1
	} else {
		return n * factorial(n-1)
	}
}

func oddOrEven(f func(x int) int, n int) string {
	r := f(n)
	fmt.Println("The factorial of",n,"is:",r)
	if (r % 2) == 0 {
		return fmt.Sprintf("The factorial of %v is an even number\n", n)
	} else {
		return fmt.Sprintf("The factorial of %v is an odd number\n", n)
	}
}
