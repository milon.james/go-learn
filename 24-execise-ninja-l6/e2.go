package main

import (
	"fmt"
)

func main() {

	ni := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
	fmt.Println(foo(1, 2, 3, 4, 5, 6, 7, 8, 9))

	fmt.Println(bar(ni))
}

func foo(xi ...int) int {
	total := 0
	for _, v := range xi {
		total += v
	}

	return total
}

func bar(yi []int) int {
	total := 0
	for _, v := range yi {
		total += v
	}

	return total
}
