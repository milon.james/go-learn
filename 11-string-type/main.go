package main

import (
	"fmt"
)

func main() {
	s := "Hello there, welcome to string world"
	fmt.Println(s)
	fmt.Printf("%T\n", s)
	// Convert STRING s to BYTE SLICE
	bs := []byte(s)
	fmt.Println(bs) // Will print the DECIMAL representation(ASCII) of each character in STRING in a BYTE SLICE. https://en.wikipedia.org/wiki/ASCII#Printable_characters
	fmt.Printf("%T\n", bs)

	// print UTF-8 codepoint. https://en.wikipedia.org/wiki/ASCII#Printable_characters
	for i := 0; i < len(s); i++ {
		fmt.Printf("%#U\n", s[i])
	}

	fmt.Println("Print index position and DECIMAL value of each character in the string")
	for i, v := range s {
		fmt.Println(i, v)
	}

	fmt.Println("Print index position and HEXADECIMAL value of each character in the string")
	for i, v := range s {
		fmt.Printf("At index position %d we have hex %#x\n", i, v)
	}

}
