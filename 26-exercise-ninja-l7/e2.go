package main

import (
	"fmt"
)

type person struct {
	first, last string
}

func changeMe(p *person) {

	p.first = "Nolim1"
	//Also (*p).first = "Nolim1" works

}

func main() {
	p := person{
		first: "Nolim",
		last:  "Mico",
	}
	fmt.Println(p.first, p.last)
	changeMe(&p)
	fmt.Println(p.first, p.last)
}
