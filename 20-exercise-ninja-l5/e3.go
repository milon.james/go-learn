package main

import (
	"fmt"
)

type vehicle struct {
	doors int
	color string
}

type truck struct {
	vehicle
	fourWheel bool
}

type sedan struct {
	vehicle
	luxury bool
}

func main() {

	ford := truck{
		vehicle: vehicle{
			doors: 2,
			color: "yellow",
		},
		fourWheel: true,
	}

	skoda := sedan{
		vehicle: vehicle{
			doors: 5,
			color: "blue",
		},
		luxury: false,
	}

	fmt.Println(ford)
	fmt.Println(ford.doors)
	fmt.Println(skoda)
	fmt.Println(skoda.color)

}
