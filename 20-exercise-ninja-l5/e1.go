package main

import (
	"fmt"
)

type person struct {
	first, last string
	fav         []string
}

func main() {

	p1 := person{
		first: "Mico",
		last:  "J",
		fav: []string{
			"vanilla",
			"strawberry",
		},
	}

	p2 := person{
		first: "Nolim",
		last:  "S",
		fav: []string{
			"choco",
			"raspberry",
		},
	}

	fmt.Println(p1.first, p1.last)
	for i, v := range p1.fav {
		fmt.Println(i, v)
	}

	fmt.Println(p2.first, p2.last)
	for i, v := range p2.fav {
		fmt.Println(i, v)
	}

}
