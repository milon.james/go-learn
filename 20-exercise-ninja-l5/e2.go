package main

import (
	"fmt"
)

type person struct {
	first, last string
	fav         []string
}

func main() {

	p1 := person{
		first: "Mico",
		last:  "J",
		fav: []string{
			"vanilla",
			"strawberry",
		},
	}

	p2 := person{
		first: "Nolim",
		last:  "S",
		fav: []string{
			"choco",
			"raspberry",
		},
	}

	m := map[string]person{
		p1.last: p1,
		p2.last: p2,
	}

	for k, v := range m {
		fmt.Println(k)
		for i, val := range v.fav {
			fmt.Println(i, val)
		}
	}

}
