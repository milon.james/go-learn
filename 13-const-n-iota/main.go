package main

import (
	"fmt"
)

const Pi float64 = 3.14159265358979323846
const zero = 0.0 // untyped floating-point constant
const (
	size int64 = 1024
	eof        = -1 // untyped integer constant
)
const a, b, c = 3, 4, "foo" // a = 3, b = 4, c = "foo", untyped integer and string constants
const d, e float32 = 0, 3   // u = 0.0, v = 3.0

const (
	u         = iota * 42 // u == 0     (untyped integer constant)
	v float64 = iota * 42 // v == 42.0  (float64 constant)
	w         = iota * 42 // w == 84    (untyped integer constant)
)

const x = iota // x == 0
const y = iota // y == 0

const (
	Sunday = iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Partyday
	numberOfDays // this constant is not exported
)

func main() {
	fmt.Println("Partyday = ", Partyday)
	fmt.Println("x = ", x)
	fmt.Println("x = ", y)
	fmt.Println("v = ", v)
	fmt.Println("eof = ", eof)
	fmt.Println("numberOfDays = ", numberOfDays)
}
