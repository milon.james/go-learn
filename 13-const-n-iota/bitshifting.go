package main

import (
	"fmt"
)

const (
	_        = iota
	kilobyte = 1 << (iota * 10) //bitwise shift 1 10 positions to the left
	megabyte = 1 << (iota * 10) //bitwise shift 1 20 positions to the left
	gigabyte = 1 << (iota * 10) //bitwise shift 1 30 positions to the left

)

func main() {
	kb := 1024
	mb := 1024 * kb
	gb := 1024 * mb

	fmt.Printf("kb\t%d\t\t%b\n", kb, kb)
	fmt.Printf("mb\t%d\t\t%b\n", mb, mb)
	fmt.Printf("gb\t%d\t%b\n", gb, gb)

	fmt.Printf("kilobyte\t%d\t\t%b\n", kilobyte, kilobyte)
	fmt.Printf("megabyte\t%d\t\t%b\n", megabyte, megabyte)
	fmt.Printf("gigabyte\t%d\t%b\n", gigabyte, gigabyte)
}
