package main

import (
	"fmt"
)

var a uint8
var b int8
func main() {
	x := 2.345678
	y := 456
	fmt.Printf("%T\n", x)
	fmt.Printf("%T\n", y)
	// a = 300 -> will result in an ERROR : constant 300 overflows uint8
	// b = -129 -> will result in an ERROR : constant -129 overflows int8
	a = 254
	b = -128
}
