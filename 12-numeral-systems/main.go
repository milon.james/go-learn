package main

import (
	"fmt"
)

func main() {
	n := 911
	fmt.Printf("%b\n", n)
	fmt.Printf("%x\n", n)
	fmt.Printf("%#X\n", n)
	
	s := "H"
	bs := []byte(s)
	fmt.Println(bs)
	i := bs[0]
	fmt.Printf("%b\n", i)
	fmt.Printf("%x\n", i)
	fmt.Printf("%#X\n", i)
	fmt.Printf("%T\n", i)
}
