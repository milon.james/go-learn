package main

import (
	"fmt"
)

func main() {

	jb := []string{"bla", "blu", "ble", "blo", "bli"}
	mp := []string{"tada", "hoo", "hee", "yeah", "foo"}

	xp := [][]string{jb, mp}
	fmt.Println(xp)
}
