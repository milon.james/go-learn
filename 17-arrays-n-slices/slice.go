package main

import (
	"fmt"
)

func main() {

	// x := type{VALUES} //COMPOSITE LITERAL
	x := []int{98, 76, 65, 54}
	fmt.Println(x[2])
	fmt.Println(x)

	// Initialize a SLICE using built in function 'make'
	fmt.Println("Initializing SLICE z using built-in function 'make'")
	z := make([]int, 10, 50) // Initialize a slice of TYPE int , LENGTH 10 and CAPACITY 50
	fmt.Println(z, len(z), cap(z))

	//Looping through a SLICE using range function
	fmt.Println("Looping through a SLICE using range function")
	for i, v := range x {
		fmt.Println(i, v)
	}

	//Looping through a SLICE using normal iteration
	fmt.Println("Looping through a SLICE using normal iteration")
	for i := 0; i < len(x); i++ {
		fmt.Println(i, x[i])
	}

	//Slicing a SLICE
	fmt.Println("Slicing a SLICE")
	fmt.Println(x[:])
	fmt.Println("Slicing a SLICE from index position 1 to the end")
	fmt.Println(x[1:])
	fmt.Println("Slicing a SLICE from index position 1 upto but not including index position 3")
	fmt.Println(x[1:3]) //Slice upto index position 3 starting from 1, but not including the position 3

	//Appending to a SLICE
	x = append(x, 87, 6, 98, 34, 21)
	fmt.Println(x)
	y := []int{32, 4, 92, 79, 41, 57}
	x = append(x, y...)
	fmt.Println(x)
	y = append(y, x[:1]...)
	fmt.Println(y)

	//Deleting from a SLICE
	fmt.Println("Deleting values in index position 2 and 3")
	x = append(x[:2], x[4:]...)
	fmt.Println(x)
}
