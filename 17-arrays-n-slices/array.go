package main

import (
	"fmt"
)

func main() {
	x := [7]int{0, 1, 2, 3, 4, 5, 6}
	fmt.Println(len(x))
	for i, v := range x {
		fmt.Println(i, v)
	}

	y := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println(len(y))
	for i, v := range y {
		fmt.Println(i, v)
	}
}
