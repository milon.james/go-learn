An array is a numbered sequence of elements of a single type, called the element type. The number of elements is called the length of the array and is never negative. 

A slice is a descriptor for a contiguous segment of an underlying array and provides access to a numbered sequence of elements from that array. A slice type denotes the set of all slices of arrays of its element type. The number of elements is called the length of the slice and is never negative. The value of an uninitialized slice is nil. 

We try to append to a SLICE with predefined capacity and when the LENGTH of the SLICE has reached it's CAPACITY, new elements
are added to a new underlying ARRAY which is a copy of existing underlying ARRAY but with double the size of the previous one.
This costs CPU processing power and hence it's always good to have the CAPACITY of the slice defined if it's known beforehand.

Aggregate data types - Arrays, Slices