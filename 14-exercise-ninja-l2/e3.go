package main

import (
	"fmt"
)

const (
	cy  = 2020 - iota
	yl1 = cy - iota
	yl2 = cy - iota
	yl3 = cy - iota
)

func main() {
	fmt.Println(cy, yl1, yl2, yl3)
}
