package main

import (
	"fmt"
)

type person struct {
	first string
	last  string
	age   int
}

type secretAgent struct {
	person
	ltk bool
}

func (s secretAgent) speak() { // In this case we have attached this function to any value of the type 'secretAgent' which is bascally a struct
	fmt.Println("I am", s.first, s.last)
}

func main() {

	p1 := secretAgent{
		person: person{
			first: "Milon",
			last:  "James",
			age:   32,
		},
		ltk: true,
	}

	p2 := secretAgent{
		person: person{
			first: "Nolim",
			last:  "Semaj",
			age:   33,
		},
		ltk: false,
	}

	p1.speak()
	p2.speak()

}
