package main

import (
	"fmt"
)

var a int

type hotdog int

var b hotdog

func main() {
	a = 42
	b = 43
	fmt.Println(a)
	fmt.Printf("%T\n", a)
	fmt.Println(b)
	fmt.Printf("%T\n", b)
	//a = b -> Not ALLOWED in GO. You cannot ASSIGN a VARIABLE of TYPE hotdog to a VARIABLE of type int. Use TYPE CONVERSION
	a = int(b)
	fmt.Println(a)
	fmt.Printf("%T\n", a)

}
