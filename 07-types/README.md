If a variable DECLARED is of a certain TYPE then it can only hold VALUES of that TYPE
Use backticks(``) to declare raw strings and it can include strings with even double quotes or newlines
Primitive DataTypes - basic or built-in like INT, STRING, BOOL etc
Composite DataTypes - Allows you to compose together values of other data TYPES. TYPES like Struct, Slice etc

You can define your own custom TYPES. See hotdog.go
TYPE CONVERSION