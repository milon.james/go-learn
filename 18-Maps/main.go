package main

import (
	"fmt"
)

func main() {

	bio := map[string]int{
		"Mico":  32,
		"Chris": 27,
	}

	fmt.Println(bio)

	fmt.Println("Checking for non-existing keys in the map")
	v, ok := bio["nolim"]
	fmt.Println(v)
	fmt.Println(ok)

	fmt.Println("Checking for non-existing keys in the map using IF conditional")

	if v, ok := bio["nolim"]; ok {
		fmt.Println(v)
	}

	if v, ok := bio["Chris"]; ok {
		fmt.Println(v)
	}

	fmt.Println("Adding a new element to the map")
	bio["Leon"] = 1

	fmt.Println("Printing the whole map using range function")
	for k, v := range bio {
		fmt.Println(k, v)
	}

	fmt.Println("Deleting an entry from the map")
	delete(bio, "Mico")
	fmt.Println(bio)

	fmt.Println("Deleting a non-existing entry from the map - will not throw any error")
	delete(bio, "Nolim")
	fmt.Println(bio)
}
