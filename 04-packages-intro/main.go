package main

import (
	"fmt"
)

func main() {

	fmt.Println("Welcome to packages in Go")

	num_bytes, err := fmt.Println("Check return values")
	fmt.Println(num_bytes, err)

	num_bytes, _ = fmt.Println("Ignore a return value")
	fmt.Println(num_bytes)

}
